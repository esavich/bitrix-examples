<?php
/**
 * деактивация разделов если все товары неактивны
 * (или имеют определенное значение свойства , статус там какой-то, например)
 */

function updateSectionByStatus()
{
    $section = new CIBlockSection();
    $allSections = [];
    $allSectionsWithUnavailable = [];
    $sectionsToDeactivate = [];

    /**
     * @var int $goodsIblockID
     * ID инфоблока с товарами
     */
    $goodsIblockID = 1;

    /**
     * Получаем все разделы с колличеством товаров
     */
    $arFilter = ['ELEMENT_SUBSECTIONS' => 'Y', 'CNT_ACTIVE' => 'Y', 'IBLOCK_ID' => $goodsIblockID];
    $sectionsObj = CIBlockSection::GetList(
        ['LEFT_MARGIN' => 'ASC'],
        $arFilter,
        true,
        ['ID', 'NAME', 'DEPTH_LEVEL', 'CODE']
    );
    while ($sectionArr = $sectionsObj->Fetch()) {
        $allSections[$sectionArr['ID']] = $sectionArr;
    }

    /**
     * Добавляем фильтрацию по свойству товара в разеделе
     * в данном примере выберем разделы в которых есть товары со свойством
     * 'STATUS_TOVARA' => 'Нет в наличии'
     */
    $arFilter['PROPERTY'] = [
        'STATUS_TOVARA' => 'Нет в наличии'
    ];
    $sectionsObj = CIBlockSection::GetList(
        ['LEFT_MARGIN' => 'ASC'],
        $arFilter,
        true,
        ['ID', 'NAME', 'DEPTH_LEVEL', 'CODE']
    );
    while ($sectionArr = $sectionsObj->Fetch()) {
        $allSectionsWithUnavailable[$sectionArr['ID']] = $sectionArr;
    }
    /**
     * пробежимся по двум получившимся массивам,
     * если общее число товаров в разделе равно числу товаров в разеделе во втором массиве (с фильтром)
     * то надо его декактивировать/изменить свойство
     */
    foreach ($allSectionsWithUnavailable as $secID => $secArr) {
        $fullSecArr = $allSections[$secID];

        if ($fullSecArr['ELEMENT_CNT'] == $secArr['ELEMENT_CNT']) {
            $sectionsToDeactivate[$secID] = $fullSecArr;
            $section->Update($secID, ["ACTIVE" => "N"]);
        }
    }

    /**
     *  так же можно проверить что просто нет активных элементов, без учета свойств
     */
    foreach ($allSections as $secArr) {
        if ($secArr["ELEMENT_CNT"] == 0) {
            $section->Update($secArr["ID"], ["ACTIVE" => "N"]);
        }
    }
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/deactivatedSections.txt', var_export($sectionsToDeactivate, true));

}