<?php
/**
 * деактивация продуктов если все SKU неактивны
 * (или имеют определенное значение свойства , статус там каокй-то, например)
 */

function updateProducts()
{
    Bitrix\Main\Loader::includeModule('iblock');

    /**
     * @var int $skuIblockID
     * ID инфоблока предложений (не товаров)
     */
    $skuIblockID = 2;

    $goodsIblockID = 1;

    $allSkus = [];
    $allSkusWithUnavailableStatus = [];
    /** @var array $goodsToDeactivate
     * массив куда запишем все деактивируемые товар (для передачи кудато далее или логирования)
     */
    $goodsToDeactivate = [];


    /**
     * выбирем вообще все SKU с группировкой по PROPERTY_CML2_LINK
     * (заменить если вдруг внезапно оно не так называется, но это дефолтное название свойства)
     */
    $allSkusObj = CIBlockElement::GetList(
        ['PROPERTY_CML2_LINK.PROPERTY_STATUS_TOVARA' => 'ASC'],
        ['IBLOCK_ID' => $skuIblockID],
        ['PROPERTY_CML2_LINK'],
        false,
        []
    );
    /**
     * отбросим SKU, не привязанные к товарам (такое бывает)
     */
    while ($skuArr = $allSkusObj->Fetch()) {
        if ($skuArr['PROPERTY_CML2_LINK_VALUE']) {
            $allSkus[$skuArr['PROPERTY_CML2_LINK_VALUE']] = $skuArr;
        }
    }

    /**
     * сделаем аналоигчную выборку, только уточним какие нам нужны предложения
     * самое распространенное это
     *
     *      'ACTIVE' => 'N'
     *
     * тут в примере выбирается по свойству статус
     */
    $arFilter = [
        'IBLOCK_ID'              => $skuIblockID,
        'PROPERTY_STATUS_TOVARA' => 'Нет в наличии',
    ];
    $skusWithUnavailableObj = CIBlockElement::GetList(
        ['PROPERTY_CML2_LINK.PROPERTY_STATUS_TOVARA' => 'ASC'],
        $arFilter,
        ['PROPERTY_CML2_LINK'],
        false,
        []
    );

    /**
     * аналогично отбросим SKU, не привязанные к товарам
     */
    while ($skuArr = $skusWithUnavailableObj->Fetch()) {
        if ($skuArr['PROPERTY_CML2_LINK_VALUE']) {
            $allSkusWithUnavailableStatus[$skuArr['PROPERTY_CML2_LINK_VALUE']] = $skuArr;
        }
    }


    /**
     * пробежимся по двум получившимся массивам,
     * если общее число предложений у товара равно числу предложений у товара во вторм массиве (с фильтром)
     * то надо его декактивировать/изменить свойство
     */
    foreach ($allSkusWithUnavailableStatus as $goodID => $skuArr) {
        $fullGoodArr = $allSkus[$goodID];
        if ($fullGoodArr['CNT'] == $skuArr['CNT'] && $skuArr['PROPERTY_CML2_LINK_PROPERTY_STATUS_TOVARA_VALUE'] != 'Нет в наличии') {
            $goodsToDeactivate[$goodID] = $fullGoodArr;
            $goodsToDeactivate[$goodID]['NA_CNT'] = $skuArr['CNT'];
            /**
             * ставим ТОВАРУ статус нет в наличии
             */
            CIBlockElement::SetPropertyValuesEx($goodID, $goodsIblockID, ['STATUS_TOVARA' => 'Нет в наличии']);
            /**
             * либо деактивируем товар
             *
             * $element = new CIBlockElement();
             * $element->Update($goodID,['ACTIVE' => 'N']);
             */
        }
    }
    /**
     * запишем измененные товары кудато в файлик
     */
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/deactivatedGoods.txt', var_export($goodsToDeactivate, true));

}
